var exec = require('cordova/exec')
var channel = require('cordova/channel');

var Constants = {
  ACTION_MEDIA_REMOVED: "android.intent.action.MEDIA_REMOVED",
  ACTION_MEDIA_UNMOUNTED: "android.intent.action.MEDIA_UNMOUNTED",
  ACTION_MEDIA_CHECKING: "android.intent.action.MEDIA_CHECKING",
  ACTION_MEDIA_NOFS: "android.intent.action.MEDIA_NOFS",
  ACTION_MEDIA_MOUNTED: "android.intent.action.MEDIA_MOUNTED",
  ACTION_MEDIA_SHARED: "android.intent.action.MEDIA_SHARED",
  ACTION_MEDIA_UNSHARED: "android.intent.action.MEDIA_UNSHARED",
  ACTION_MEDIA_BAD_REMOVAL: "android.intent.action.MEDIA_BAD_REMOVAL",
  ACTION_MEDIA_UNMOUNTABLE: "android.intent.action.MEDIA_UNMOUNTABLE",
  ACTION_MEDIA_EJECT: "android.intent.action.MEDIA_EJECT"
}

channel.create('onRemovableMediaAction')
channel.onCordovaReady.subscribe(function() {
  var actions = Object.keys(Constants).map(function(k) { return Constants[k] })
  exec(
    function (action) {
      console.log('got action', action)
      if (action) {
        window.cordova.fireWindowEvent("onRemovableMediaAction", action);
      }
    },
    function (error) { console.log("Storage media receiver error", error) },
    'RemovableStorage', 'startReceiver', actions
  )
})

module.exports = {
  Constants: Constants,

  getUsbDevices: function (successHandler, errorHandler) {
    exec(
      successHandler,
      errorHandler,
      'RemovableStorage', 'getUsbDevices', []
    )
  },

  getMountInfo: function (successHandler, errorHandler) {
    exec(
      successHandler,
      errorHandler,
      'RemovableStorage', 'getMountInfo', []
    )
  },

  getEnvironment: function (successHandler, errorHandler) {
    exec(
      successHandler,
      errorHandler,
      'RemovableStorage', 'getEnvironment', []
    )
  }
}
