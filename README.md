# Cordova plugin for removable storage media

__This plugin is only for android__

This cordova plugin provides a couple of helpers methods to read info about connected devices, mount points and adds an event emitter for android media intents.

We are using Android 4 atm and the mount point of a usb storage device cannot be read through the Android SDK. We have to rely on reading the info from "/proc/self/mountinfo" and parsing this to find the path to where the device was mounted in the filesystem. Since this is device/manufacturer specific, it is left to the app to filter the mount info for the mount points of removable mass storage. This plugin just provides helpers methods for javascript to get access to the information.

It also forwards some Intents as events. E.g. the `ACTION_MEDIA_MOUNTED` intent does carry the path where the media was mounted. But since this is fired when the usb device is mounted and the app might not run at that point, the app might not receive the intent and we cannot rely on using just the intents. Though, one way could be to write a service that is recording these events and track if and where a device is currently mounted. That service would always run in the background. But this is currently out of scope.

The plugin will globally register through cordova and can be accessed at:

`window.removableStorage`

## API

### Methods
`getMountInfo(successCallback, errorCallback)`

Returns the list of all mount points. The list is gathered from reading "/proc/self/mountinfo". The list can be used to find the mount point of a usb storage device. The name of the folders where the device is mounted are manufacturer specific and the app should do the filtering itself.

`GetUsbDevices(successCallback, errorCallback)`

Returns the list of connected USB devices. It provides some generic info about the device and also if this is a mass storage device. It cannot, though, tell where this was mounted in the filesystem, only in the usbfs.

`getEnvironment(successCallback, errorCallback)`

Get paths that are provided by the android Environment and Intent class.

### Events

The following events are provided. These are analogue to the corresponding android intents. The event action types can be got from "removableStorage.Constants"

```
ACTION_MEDIA_REMOVED
ACTION_MEDIA_UNMOUNTED
ACTION_MEDIA_CHECKING
ACTION_MEDIA_NOFS
ACTION_MEDIA_MOUNTED
ACTION_MEDIA_SHARED
ACTION_MEDIA_UNSHARED
ACTION_MEDIA_BAD_REMOVAL
ACTION_MEDIA_UNMOUNTABLE
ACTION_MEDIA_EJECT
```

Register an event handler with "onRemovableMediaAction", e.g.

```
window.addEventListener("onRemovableMediaAction", (event) => {
  switch (event.action) {
    case removableStorage.Constants.ACTION_MEDIA_MOUNTED
      console.log("Media mounted")
  }
})
```