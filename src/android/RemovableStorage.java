package org.ehealthafrica.cordova;

import java.util.Set;
import java.util.HashMap;
import java.util.List;
import java.util.Arrays;
import java.util.StringTokenizer;
import java.util.NoSuchElementException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.File;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.BroadcastReceiver;
import android.util.Log;
import android.os.Bundle;
import android.hardware.usb.UsbManager;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbEndpoint;
import android.hardware.usb.UsbConstants;
import android.os.Environment;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.PluginResult;

import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;


public class RemovableStorage extends CordovaPlugin {

    private static final String TAG = "CordovaRemovableStorage";

    BroadcastReceiver receiver = null;
    private CallbackContext callbackContext = null;

    UsbManager usbManager = null;

    private final String[] intentActions = {
        Intent.ACTION_MEDIA_BAD_REMOVAL,
        Intent.ACTION_MEDIA_BUTTON ,
        Intent.ACTION_MEDIA_CHECKING ,
        Intent.ACTION_MEDIA_EJECT,
        Intent.ACTION_MEDIA_MOUNTED,
        Intent.ACTION_MEDIA_NOFS ,
        Intent.ACTION_MEDIA_REMOVED,
        Intent.ACTION_MEDIA_SCANNER_FINISHED ,
        Intent.ACTION_MEDIA_SCANNER_SCAN_FILE,
        Intent.ACTION_MEDIA_SCANNER_STARTED,
        Intent.ACTION_MEDIA_SHARED ,
        Intent.ACTION_MEDIA_UNMOUNTABLE,
        Intent.ACTION_MEDIA_UNMOUNTED
    };

    @Override
    public boolean execute(String action,
                           JSONArray data,
                           final CallbackContext cbContext) throws JSONException {

        if (usbManager == null) {
            usbManager = (UsbManager)webView.getContext().getSystemService(Context.USB_SERVICE);
        }

        if (action.equals("getUsbDevices")) {
            try {
                cbContext.success(getUsbDevices());
            } catch (Exception e) {
                Log.e(TAG, "Error getting usb devices: " + e.getMessage(), e);
                cbContext.error(e.getMessage());
            }
            return true;

        } else if (action.equals("getMountInfo")) {
            try {
                cbContext.success(getMountInfo());
            } catch (Exception e) {
                Log.e(TAG, "Error getting mounts: " + e.getMessage(), e);
                cbContext.error(e.getMessage());
            }
            return true;

        } else if (action.equals("getEnvironment")) {
            try {
                cbContext.success(getEnvironment());
            } catch (Exception e) {
                Log.e(TAG, "Error getting the environment: " + e.getMessage(), e);
                cbContext.error(e.getMessage());
            }
            return true;

        } else if (action.equals("startReceiver")) {
            if (this.callbackContext != null) {
                Log.v(TAG, "Receiver already running");
                this.callbackContext = cbContext;
                return true;
            }
            this.callbackContext = cbContext;
            registerReceiver();

            PluginResult result = new PluginResult(PluginResult.Status.OK);
            result.setKeepCallback(true);
            this.callbackContext.sendPluginResult(result);
            return true;

        } else if (action.equals("stopReceiver")) {
            unregisterReceiver();
            sendAction(new JSONObject(), false);
            this.callbackContext = null;
            callbackContext.success();
            return true;
        }
        return false;
    }

    public void onDestroy() {
        unregisterReceiver();
    }

    public void onReset() {
        unregisterReceiver();
    }

    private JSONArray getUsbDevices() throws JSONException {
        HashMap<String, UsbDevice> devices = usbManager.getDeviceList();
        JSONArray devicesJson = new JSONArray();
        for (UsbDevice device: devices.values()) {
            JSONObject deviceJson = new JSONObject();
            deviceJson.put("id", device.getDeviceId());
            deviceJson.put("type", device.getDeviceClass());
            // Android docstring for getDeviceName:
            //     Returns the name of the device. In the standard implementation, this is
            //     the path of the device file for the device in the usbfs file system.
            // Sadly this is not the device that will get listed in mountInfo.
            deviceJson.put("name", device.getDeviceName());
            deviceJson.put("vendorId", device.getVendorId());
            deviceJson.put("productId", device.getProductId());

            boolean isMassStorage = false;

            JSONArray ifacesJson = new JSONArray();
            for (int i = 0; i < device.getInterfaceCount(); ++i) {
                JSONObject ifaceJson = new JSONObject();
                UsbInterface iface = device.getInterface(i);
                ifaceJson.put("id", iface.getId());
                ifaceJson.put("type", iface.getInterfaceClass());
                ifaceJson.put("protocol", iface.getInterfaceProtocol());

                if (!isMassStorage) {
                    isMassStorage = iface.getInterfaceClass() == UsbConstants.USB_CLASS_MASS_STORAGE;
                }

                JSONArray endpointsJson = new JSONArray();
                for (int j = 0; j < iface.getEndpointCount(); ++j) {
                    JSONObject endpointJson = new JSONObject();
                    UsbEndpoint endpoint = iface.getEndpoint(j);
                    endpointJson.put("address", endpoint.getAddress());
                    endpointJson.put("attributes", endpoint.getAttributes());
                    endpointJson.put("direction", endpoint.getDirection());
                    endpointsJson.put(endpointJson);
                }
                ifaceJson.put("endpoints", endpointsJson);
                ifacesJson.put(ifaceJson);
            }
            deviceJson.put("interfaces", ifacesJson);
            deviceJson.put("isMassStorage", isMassStorage);
            devicesJson.put(deviceJson);
        }
        return devicesJson;
    }

    private JSONObject getEnvironment() throws JSONException {
        Context context = webView.getContext();
        JSONObject result = new JSONObject();
        result.put("dataDirectory", Environment.getDataDirectory().getAbsolutePath());
        result.put("externalStorageDirectory", Environment.getExternalStorageDirectory().getAbsolutePath());
        result.put("externalStorageState", Environment.getExternalStorageState());
        result.put("externalCacheDir", context.getExternalCacheDir().getAbsolutePath());
        result.put("externalFilesDir", context.getExternalFilesDir(null).getAbsolutePath());

        JSONArray externalCacheDirs = new JSONArray();
        for (File file : context.getExternalCacheDirs()) {
            if (file != null) {
                externalCacheDirs.put(file.getAbsolutePath());
            }
        }
        result.put("externalCacheDirs", externalCacheDirs);

        JSONArray externalFilesDirs = new JSONArray();
        for (File file : context.getExternalFilesDirs(null)) {
            if (file != null) {
                externalFilesDirs.put(file.getAbsolutePath());
            }
        }
        result.put("externalFilesDirs", externalFilesDirs);
        return result;
    }

    private void sendAction(JSONObject info, boolean keepCallback) {
        if (this.callbackContext != null) {
            PluginResult result = new PluginResult(PluginResult.Status.OK, info);
            result.setKeepCallback(keepCallback);
            this.callbackContext.sendPluginResult(result);
        }
    }

    private void sendError(Exception e) {
        if (this.callbackContext != null) {
            PluginResult result = new PluginResult(PluginResult.Status.ERROR, e.getMessage());
            result.setKeepCallback(true);
            this.callbackContext.sendPluginResult(result);
        }
    }

    private JSONObject getIntentInfo(Intent intent) throws JSONException {
        JSONObject obj = new JSONObject();
        obj.put("action", intent.getAction());
        obj.put("data", intent.getData());

        Bundle bundle = intent.getExtras();
        JSONObject extras = new JSONObject();
        if (bundle != null) {
            Set<String> keys = bundle.keySet();
            for (String key : keys) {
                extras.put(key, JSONObject.wrap(bundle.get(key)));
            }
            obj.put("extras", extras);
        }
        return obj;
    }

    private void registerReceiver() {
        if (this.receiver == null) {
            this.receiver = new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        try {
                            sendAction(getIntentInfo(intent), true);
                        } catch (Exception e) {
                            Log.e(TAG, "Error sending info json: " + e.getMessage(), e);
                            sendError(e);
                        }
                    }
                };
            IntentFilter intentFilter = new IntentFilter();
            for (String a : intentActions) {
                intentFilter.addAction(a);
            }
            // This is required for receiving ACTION_MEDIA_* intents!
            intentFilter.addDataScheme("file");

            webView.getContext().registerReceiver(this.receiver, intentFilter);
        }
    }

    private void unregisterReceiver() {
        if (this.receiver != null) {
            try {
                webView.getContext().unregisterReceiver(this.receiver);
                this.receiver = null;
            } catch (Exception e) {
                Log.e(TAG, "Error unregistering receiver: " + e.getMessage(), e);
                sendError(e);
            }
        }
    }

    private JSONArray getMountInfo() throws JSONException {
        // see format description: https://www.kernel.org/doc/Documentation/filesystems/proc.txt
        String mountinfoFile = "/proc/self/mountinfo";
        String keysSeperator = " - ";
        // keys are in order of appereance
        String[][] keys = {
            {
                "mountId",
                "parentId",
                "majorMinor",
                "root",
                "mountPoint",
                "mountOptions",
                // we skip the optional fields
            },
            // the keys are seperated by a seperator token
            {
                "filesystemType",
                "mountSource",
                "superOptions"
            }
        };
        JSONArray mounts = new JSONArray();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(mountinfoFile));
            String line;
            while ((line = reader.readLine()) != null) {
                JSONObject mount = new JSONObject();
                String[] parts = line.split(keysSeperator);

                for (int i = 0; i < parts.length && i < keys.length; ++i) {
                    String[] fields = parts[i].split(" ");

                    for (int j = 0; j < fields.length && j < keys[i].length; ++j) {
                        String key = keys[i][j];
                        String value = fields[j];
                        mount.put(key, value);
                    }
                }
                mounts.put(mount);
            }
        } catch (FileNotFoundException ex) {
            Log.e(TAG, "File not found: " + ex.getMessage(), ex);
            sendError(ex);
        } catch (IOException ex) {
            Log.e(TAG, "IO error: " + ex.getMessage(), ex);
            sendError(ex);
        } catch (NoSuchElementException ex) {
            Log.e(TAG, "No more tokens: " + ex.getMessage(), ex);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException ex) {}
            }
        }
        return mounts;
    }
}
